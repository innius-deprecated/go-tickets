package ticket

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestActiveCount(t *testing.T) {
	p := NewTicketPool(3)
	assert.Equal(t, 3, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
	ticket := p.GetTicket()
	assert.Equal(t, 1, p.ActiveCount())
	p.Return(nil)
	assert.Equal(t, 1, p.ActiveCount())
	p.Return(ticket)
	assert.Equal(t, 0, p.ActiveCount())
}

func TestTicketPoolResizeInUse(t *testing.T) {
	p := NewTicketPool(3)
	assert.Equal(t, 0, p.ActiveCount())
	ticket := p.GetTicket()
	assert.Equal(t, 1, p.ActiveCount())
	p.Resize(2)
	assert.Equal(t, 2, p.Size())
	assert.Equal(t, 1, p.ActiveCount())
	p.Return(ticket)
	assert.Equal(t, 0, p.ActiveCount())
}

func TestTicketPoolResizeTooSmall(t *testing.T) {
	p := NewTicketPool(3)
	assert.Equal(t, 0, p.ActiveCount())
	ticketa := p.GetTicket()
	ticketb := p.GetTicket()
	assert.Equal(t, 2, p.ActiveCount())
	p.Resize(1)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 2, p.ActiveCount())
	p.Return(ticketa)
	p.Return(ticketb)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
}

func TestTicketPoolForceResize(t *testing.T) {
	p := NewTicketPool(3)
	assert.Equal(t, 0, p.ActiveCount())
	ticketa := p.GetTicket()
	ticketb := p.GetTicket()
	assert.Equal(t, 2, p.ActiveCount())
	p.Resize(1)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 2, p.ActiveCount())
	p.Return(ticketa)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 1, p.ActiveCount())
	p.Return(ticketb)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
}

func TestMinimumPoolSize(t *testing.T) {
	p := NewTicketPool(-5)
	assert.Equal(t, 1, p.Size())
	p.Resize(-50)
	assert.Equal(t, 1, p.Size())
}

func TestPercentageInUse(t *testing.T) {
	p := NewTicketPool(11)
	assert.Equal(t, 0, p.PercentageUsed())
	p.GetTicket()
	p.GetTicket()
	p.GetTicket()
	assert.Equal(t, 27, p.PercentageUsed())
}

func TestDoubleForceResize(t *testing.T) {
	p := NewTicketPool(3)
	assert.Equal(t, 3, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
	ticketa := p.GetTicket()
	ticketb := p.GetTicket()
	ticketc := p.GetTicket()
	assert.Equal(t, 3, p.Size())
	assert.Equal(t, 3, p.ActiveCount())
	p.Resize(2)
	assert.Equal(t, 2, p.Size())
	assert.Equal(t, 3, p.ActiveCount())
	p.Resize(1)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 3, p.ActiveCount())
	p.Return(ticketa)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 2, p.ActiveCount())
	p.Return(ticketb)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 1, p.ActiveCount())
	p.Return(ticketc)
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
}

func TestResizeToSmaller(t *testing.T) {
	p := NewTicketPool(2)
	t1 := p.GetTicket()
	t2 := p.GetTicket()
	p.Resize(1)
	p.Return(t1)
	p.Return(t2)
	t1 = p.GetTicket()
	t2 = p.GetTicket()
	assert.NotNil(t, t1)
	assert.Nil(t, t2)
}
