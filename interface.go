package ticket

type Ticket interface {
}

type TicketPool interface {
	// Resize a ticket pool, introducing a new hard limit.
	// In other words: resizing from 3 -> 1 with 2 tickets in use will result in a new size of 1.
	// The first ticket that is returned that signals overprovisioning (2 in use -> 1 in use) will be
	// destroyed since it exceeded the newly assigned capacity.
	Resize(n int)
	// Return a ticket for further use.
	Return(ticket Ticket)
	// Get a ticket. Nil if failed.
	GetTicket() Ticket
	// The amount of tickets currently in use.
	ActiveCount() int
	// The current size (capacity) of this pool
	Size() int
	// The percentage of tickets that's currently in use.
	PercentageUsed() int
}
