package ticket

import (
	"sync"
)

type ticket *struct{}

type ticket_pool struct {
	max       int
	tickets   chan Ticket
	lock      sync.Mutex
	excessive chan Ticket
}

const min_pool_size int = 1

// Create a new ticket pool with a given max size.
func NewTicketPool(max int) TicketPool {
	if max < min_pool_size {
		// a pool has a minimum of size
		max = min_pool_size
	}

	p := &ticket_pool{
		max: max,
	}

	p.tickets = make(chan Ticket, max)
	p.excessive = make(chan Ticket, 0)
	for i := 0; i < max; i++ {
		p.tickets <- &struct{}{}
	}

	return p
}

func (p *ticket_pool) Resize(n int) {
	if n < min_pool_size {
		// dont resize to something < 1
		return
	}
	p.lock.Lock()
	defer p.lock.Unlock()

	active := p.ActiveCount()

	alloc_cnt := n - active
	if alloc_cnt < 0 {
		excessive := alloc_cnt * -1
		// a channel that serves as a counter with excessive tickets
		p.excessive = make(chan Ticket, excessive)
		for i := 0; i < excessive; i++ {
			p.excessive <- &struct{}{}
		}

	}

	p.max = n
	p.tickets = make(chan Ticket, p.max)

	for i := 0; i < alloc_cnt; i++ {
		p.tickets <- &struct{}{}
	}
}

func (p *ticket_pool) Return(ticket Ticket) {
	if ticket == nil {
		return
	}
	select {
	case _ = <-p.excessive:
		return
	default:
		p.tickets <- ticket
	}
}

func (p *ticket_pool) GetTicket() Ticket {
	var t Ticket = nil

	select {
	case t = <-p.tickets:
	default:
	}

	return t
}

func (p *ticket_pool) ActiveCount() int {
	return p.max - len(p.tickets) + len(p.excessive)
}

func (p *ticket_pool) Size() int {
	return p.max
}

func (p *ticket_pool) PercentageUsed() int {
	return p.ActiveCount() * 100 / p.max
}
